/* main.c */

#include <string.h>

#include "ast.h"
#include "ring.h"
#include "tokenizer.h"

void ast_print(struct AST *ast) { print_ast(stdout, ast); }
void token_print(struct token token) { printf("%s(%" PRId64 ")", TOKENS_STR[token.type], token.value); }

DECLARE_RING(ast, struct AST*)
DEFINE_RING(ast, struct AST*)
DEFINE_RING_PRINT(ast, ast_print)
DEFINE_RING(token, struct token)
DEFINE_RING_PRINT(token, token_print)

#define RETURN_ERROR(code, msg) return printf(msg), code

#define MAX_LEN 1024

void push_op_to_ast(struct ring_ast **ast, struct ring_token **ops) {

  struct token op = ring_token_pop_top(ops);
  if(op.type == TOK_MINUS || op.type == TOK_PLUS || op.type == TOK_DIV || op.type == TOK_MUL) {
    struct AST *rhs = ring_ast_pop_top(ast);
    struct AST *lhs = ring_ast_pop_top(ast);

    if(op.type == TOK_MINUS) ring_ast_push_top(ast, binop(BIN_MINUS, lhs, rhs));
    if(op.type == TOK_PLUS) ring_ast_push_top(ast, binop(BIN_PLUS, lhs, rhs));
    if(op.type == TOK_DIV) ring_ast_push_top(ast, binop(BIN_DIV, lhs, rhs));
    if(op.type == TOK_MUL) ring_ast_push_top(ast, binop(BIN_MUL, lhs, rhs));
  } else if(op.type == TOK_NEG) {
    struct AST *top = ring_ast_pop_top(ast);

    if(op.type == TOK_NEG) ring_ast_push_top(ast, unop(UN_NEG, top));
  }
}

struct AST *build_ast(char *str)
{
  struct ring_token *tokens = NULL;
  if ((tokens = tokenize(str)) == NULL)
    RETURN_ERROR(NULL, "Tokenization error.\n");

  struct ring_ast *ast = NULL;
  struct ring_token *ops = NULL;

  ring_token_print(tokens);

  while(tokens != NULL) {
    struct token next = ring_token_pop_top(&tokens);
    switch (next.type)
    {
      case TOK_LIT: {
        ring_ast_push_top(&ast, lit(next.value));
        break;
      }
      case TOK_OPEN: {
        ring_token_push_top(&ops, next);
        break;
      }
      case TOK_CLOSE: {
        while(ops != NULL && ring_token_first(ops).type != TOK_OPEN) {
          push_op_to_ast(&ast, &ops);
        }
        ring_token_pop_top(&ops);
        break;
      }
      default:{
        if(!ops) {
          ring_token_push_top(&ops, next);
          continue;
        }

        if(OP_PRIORITY[next.type] <= OP_PRIORITY[ring_token_first(ops).type]) {
          while(ops != NULL && OP_PRIORITY[next.type] <= OP_PRIORITY[ring_token_first(ops).type]) {
            push_op_to_ast(&ast, &ops);
          }
        }
        
        ring_token_push_top(&ops, next);
        break;
      }
    }
  }

  while(ops != NULL) {
    push_op_to_ast(&ast, &ops);
  }

  ring_token_free(&tokens);

  return ring_ast_pop_top(&ast);
}


int main()
{
  /* char *str = "1 + 2 * (2 - -3) + 8"; */
  char str[MAX_LEN];
  if (fgets(str, MAX_LEN, stdin) == NULL)
    RETURN_ERROR(0, "Input is empty.");

  if (str[strlen(str) - 1] == '\n')
    str[strlen(str) - 1] = '\0';

  struct AST *ast = build_ast(str);

  if (ast == NULL)
    printf("AST build error.\n");
  else
  {
    print_ast(stdout, ast);
    printf("\n\n%s = %" PRId64 "\n", str, calc_ast(ast));
    p_print_ast(stdout, ast);
    printf(" = %" PRId64 "\n", calc_ast(ast));    
  }

  return 0;
}
